<?php

/**
 * @link      http://industrialax.com/
 * @email     xristmas365@gmail.com
 * @author    isteil
 * @copyright Copyright (c) 2020 INDUSTRIALAX SOLUTIONS LLC
 * @license   https://industrialax.com/license
 */

use app\modules\user\models\User;

return [
    User::ROLE_USER    => [
        'name' => 'User',
    ],
    User::ROLE_TRAINER => [
        'name'  => 'Trainer',
        'items' => [
            User::ROLE_USER,
        ],
    ],
    User::ROLE_MANAGER => [
        'name'  => 'Manager',
        'items' => [
            User::ROLE_TRAINER,
        ],
    ],
    User::ROLE_ADMIN   => [
        'name'  => 'Administrator',
        'items' => [
            User::ROLE_MANAGER,
        ],
    ],
];
