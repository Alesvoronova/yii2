<?php

return [
	'admin'      => 'app\modules\admin\Module',
	'user'       => 'app\modules\user\Module',
	'settings'   => 'app\modules\settings\Module',
	'system'     => 'app\modules\system\Module',
	'gym'        => 'app\modules\gym\Module',
	'invitation' => 'app\modules\invitation\Module',
	'type'       => 'app\modules\type\Module',
	'group'      => 'app\modules\group\Module',
	'schedule'   => 'app\modules\schedule\Module',
	'gridview'   => 'kartik\grid\Module',

];
