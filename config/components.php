<?php

/**
 * @link      http://industrialax.com/
 * @email     xristmas365@gmail.com
 * @author    isteil
 * @copyright Copyright (c) 2020 INDUSTRIALAX SOLUTIONS LLC
 * @license   https://industrialax.com/license
 */

use app\modules\user\models\User;
use app\modules\user\components\AuthManager;
use app\modules\settings\components\Settings;

return [
    'request'      => [
        'cookieValidationKey' => 'yL1FydCIbAwf71v9wqUJb7ZSFkn6iIFB',
    ],
    'user'         => [
        'identityClass' => User::class,
        'loginUrl'      => ['sign/in'],
        'accessChecker' => AuthManager::class,
    ],
    'errorHandler' => [
        'errorAction' => 'site/error',
    ],
    'log'          => [
        'traceLevel' => YII_DEBUG ? 3 : 0,
        'targets'    => [
            [
                'class'  => 'yii\log\DbTarget',
                'levels' => ['error'],
            ],
        ],
    ],
    'db'           => require __DIR__ . '/db.php',
    'urlManager'   => [
        'enablePrettyUrl' => true,
        'showScriptName'  => false,
        'rules'           => require __DIR__ . '/urls.php',
    ],
    'formatter'    => [
        'nullDisplay'    => '',
        'datetimeFormat' => 'MM/dd/yyyy HH:mm a',
        'dateFormat'     => 'MM/dd/yyyy',
        //'timeZone'       => 'America/Los_Angeles',
        'currencyCode'   => 'usd',
    ],
    'settings'     => ['class' => Settings::class],
    'assetManager' => [
        'forceCopy' => true,
        'class'     => 'yii\web\AssetManager',
        'bundles'   => [
            'yii\web\JqueryAsset'                 => ['js' => ['jquery.min.js']],
            'yii\bootstrap4\BootstrapAsset'       => ['class' => 'app\modules\admin\assets\BootstrapAsset'],
            'yii\bootstrap4\BootstrapPluginAsset' => ['class' => 'app\modules\admin\assets\BootstrapPluginAsset'],
        ],

    ],

];
