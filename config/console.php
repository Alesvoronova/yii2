<?php

$config = [
	'id'            => 'basic',
	'name'          => 'IndustrialAX',
	'basePath'      => dirname(__DIR__),
	'bootstrap'     => ['log'],
	'aliases'       => [
		'@bower' => '@vendor/bower-asset',
		'@npm'   => '@vendor/npm-asset',
	],
	'components'    => [
		'cache' => [
			'class' => 'yii\caching\FileCache',
		],
		'log'   => [
			'targets' => [
				[
					'class'  => 'yii\log\DbTarget',
					'levels' => ['error', 'warning'],
				],
			],
		],
		'db'    => require __DIR__ . '/db.php',
	],
	'params'        => $params = require __DIR__ . '/params.php',
	'controllerMap' => [
		'migrate' => [
			'class'         => 'yii\console\controllers\MigrateController',
			'migrationPath' => [
				'@yii/log/migrations/',
				'@app/migrations',
				'@app/modules/user/migrations',
				'@app/modules/settings/migrations',
				'@app/modules/admin/migrations',
				'@app/modules/gym/migrations',
				'@app/modules/invitation/migrations',
				'@app/modules/type/migrations',
				'@app/modules/group/migrations',
				'@app/modules/schedule/migrations',
			],
		],
	],

];

if(YII_ENV_DEV) {
	$config['bootstrap'][] = 'gii';
	$config['modules']['gii'] = [
		'class' => 'yii\gii\Module',
	];
}

return $config;
