const webpack = require('webpack');
const merge = require('webpack-merge');
const defaultConfig = require('./webpack.config.js');
const MiniCssExtractPlugin = require('mini-css-extract-plugin');

module.exports = merge(defaultConfig, {
  mode: 'development',
  devServer: {
    overlay: true
  },
  plugins: [
    new MiniCssExtractPlugin({
      filename: '[name].css',
      // chunkFilename: '[id].css'
    }),
    // new webpack.ProvidePlugin({
    //   $: 'jquery',
    //   jQuery: 'jquery'
    // }),
    new webpack.SourceMapDevToolPlugin({
      filename: "[file].map"
    })
  ]
});
