<?php
/**
 * @link      http://industrialax.com/
 * @email     xristmas365@gmail.com
 * @author    isteil
 * @copyright Copyright (c) 2020 INDUSTRIALAX SOLUTIONS LLC
 * @license   https://industrialax.com/license
 */

namespace app\controllers;

use Yii;
use yii\web\Controller;

class SiteController extends Controller
{
	
	/**
	 * Displays Index Page.
	 *
	 * @return string
	 */
	public function actionIndex()
	{
		/**
		 * SEO tags
		 */
		$view = Yii::$app->view;
		$view->title = Yii::$app->settings->main_title;
		$view->registerMetaTag(['name' => 'description', 'content' => Yii::$app->settings->main_description]);
		$view->registerMetaTag(['name' => 'keywords', 'content' => Yii::$app->settings->main_keywords]);
		
		return $this->render('index');
	}
	
	public function actionError()
	{
		$this->layout = YII_ENV == 'dev' ? false : 'main';
		$path = YII_ENV == 'dev' ? '@yii/views/errorHandler/exception.php' : '404';
		
		return $this->render($path, [
			'handler'   => Yii::$app->errorHandler,
			'exception' => Yii::$app->errorHandler->exception,
		]);
	}
}
