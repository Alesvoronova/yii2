<?php
/**
 * @link      http://industrialax.com/
 * @email     xristmas365@gmail.com
 * @author    isteil
 * @copyright Copyright (c) 2020 INDUSTRIALAX SOLUTIONS LLC
 * @license   https://industrialax.com/license
 */

namespace app\controllers;

use app\modules\gym\models\Gym;
use app\modules\user\models\TrainerUp;
use app\modules\user\models\UserUp;
use Yii;
use yii\web\Response;
use yii\web\Controller;
use app\modules\user\models\SignIn;
use app\modules\user\models\SignUp;

class SignController extends Controller
{

    /**
     * Displays Index Page.
     *
     * @return string
     */
    public function actionIn()
    {
        $model = new SignIn();

        if($model->load(Yii::$app->request->post()) && $model->in()) {
            return $this->redirect(Yii::$app->getModule('user')->adminUrl);
        }

        return $this->render('in', ['model' => $model]);
    }

    /**
     * Sign up action.
     *
     * @return string|Response
     */
    public function actionUp()
    {
        $model = new SignUp;

        if($model->load(Yii::$app->request->post())) {
            if($model->signUp()) {

                return $this->redirect(['/admin/dashboard/index']);
            }

        }

        return $this->render('up', ['model' => $model]);
    }
	
	/**
	 * Sign up action.
	 *
	 * @return string|Response
	 */
	public function actionTrainerUp($auth)
	{
		$model = new TrainerUp;
		
		if($model->load(Yii::$app->request->post())) {
			if($model->signUp($auth)) {
		
				return $this->redirect(['/admin/dashboard/index']);
			}
		
		}
		
		return $this->render('trainerUp', ['model' => $model]);
	}
	
	/**
	 * Sign up action.
	 *
	 * @return string|Response
	 */
	public function actionJoin()
	{
		$model = new UserUp;
		$gym = Gym::items();
		
		if($model->load(Yii::$app->request->post())) {
			if($model->signUp()) {
				
				return $this->redirect(['/admin/dashboard/index']);
			}
			
		}
		
		return $this->render('userUp', [
			'model' => $model,
			'gym' => $gym,
		]);
	}

    /**
     * User Sign Out
     *
     * @return string
     */
    public function actionOut()
    {
        Yii::$app->user->logout();

        return $this->goHome();
    }
}
