<?php
/**
 * @link      http://industrialax.com/
 * @email     xristmas365@gmail.com
 * @author    isteil
 * @copyright Copyright (c) 2020 INDUSTRIALAX SOLUTIONS LLC
 * @license   https://industrialax.com/license
 */

namespace app\controllers;

use yii\web\Controller;

class LiveController extends Controller
{

    /**
     * Displays Index Page.
     *
     * @return string
     */
    public function actionStream()
    {

    }
}
