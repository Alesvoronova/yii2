import '../scss/main.scss';

const btn = $('#to-top');
const $window = $(window);
const $document = $(document);

$window.scroll(() => {
  if ($window.scrollTop() > 300) {
    btn.addClass('show');
  } else {
    btn.removeClass('show');
  }
});

$document.on('click', '#to-top', (e) => {
  e.preventDefault();
  $('html, body').animate({ scrollTop: 0 }, '300');
});

$document.on('change', '.page-sizer', function () {
  const size = $(this).val();
  const url = $(this).data('url');
  $.get(`${url}?size=${size}`).done(() => {
    location.reload();
  });
})
