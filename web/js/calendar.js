
const calendarEl = document.getElementById('calendar')

const calendar = new FullCalendar.Calendar(calendarEl, {
    plugins: ['dayGrid', 'timeGrid', 'list', 'bootstrap', 'interaction'],
    events: '/schedule/schedule/date',
    defaultView: 'dayGridMonth',
    themeSystem: 'bootstrap',
    eventLimit: true,
    header: {
        left: 'dayGridMonth,timeGridWeek,timeGridDay',
        center: 'title',
        right: 'prevYear,prev,next,nextYear'
    },
});

calendar.render()
