<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%gym}}`.
 */
class m200323_190332_create_gym_table extends Migration
{
	
	public $table = '{{%gym}}';
	
	/**
	 * {@inheritdoc}
	 */
	public function safeUp()
	{
		$this->createTable($this->table, [
			'id'         => $this->primaryKey(),
			'name'       => $this->string()->notNull()->unique(),
			'address'    => $this->string(),
			'city'       => $this->string(),
			'state'      => $this->string()->notNull()->defaultValue('CA'),
			'zip'        => $this->integer(),
			'phone'      => $this->string(),
			'slug'       => $this->string(),
			'manager_id' => $this->integer(),
			'created_at' => $this->integer(),
			'updated_at' => $this->integer(),
			'auth'       => $this->string(),
		]);
		
		$this->addForeignKey(
			'fk-user-gym_manager_id',
			$this->table,
			'manager_id',
			'user',
			'id',
			'SET NULL',
			'NO ACTION'
		);
	}
	
	/**
	 * {@inheritdoc}
	 */
	public function safeDown()
	{
		$this->dropForeignKey(
			'fk-user-gym_manager_id',
			$this->table
		);
		
		$this->dropTable($this->table);
	}
}
