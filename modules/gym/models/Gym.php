<?php

namespace app\modules\gym\models;

use app\modules\user\models\User;
use yii\behaviors\SluggableBehavior;
use yii\behaviors\TimestampBehavior;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "gym".
 *
 * @property int         $id
 * @property string|null $name
 * @property string|null $address
 * @property string|null $city
 * @property string      $state
 * @property int|null    $zip
 * @property string|null $phone
 * @property string|null $slug
 * @property int|null    $manager_id
 * @property int|null    $created_at
 * @property int|null    $updated_at
 * @property string|null $auth
 *
 * @property User        $manager
 */
class Gym extends \yii\db\ActiveRecord
{

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'gym';
    }

    public function behaviors()
    {
        return [
            TimestampBehavior::class,
            'slug' => [
                'class'         => SluggableBehavior::class,
                'attribute'     => ['name'],
                'slugAttribute' => 'slug',
                'immutable'     => false,
                'ensureUnique'  => true,
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['zip', 'manager_id', 'created_at', 'updated_at'], 'default', 'value' => null],
            [['zip', 'manager_id', 'created_at', 'updated_at'], 'integer'],
            [['state'], 'default', 'value' => 'CA'],
            [['name', 'address', 'city', 'state', 'phone', 'slug', 'auth'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id'         => 'ID',
            'name'       => 'Name',
            'address'    => 'Address',
            'city'       => 'City',
            'state'      => 'State',
            'zip'        => 'Zip',
            'phone'      => 'Phone',
            'slug'       => 'Slug',
            'manager_id' => 'Manager ID',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
            'auth'       => 'Auth',
        ];
    }

    /**
     * Gets query for [[Manager]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getManager()
    {
        return $this->hasOne(User::class, ['id' => 'manager_id']);
    }
	
	/**
	 * Get Items ['ID'=> 'NAME', ..]
	 *
	 * @return array
	 */
	public static function items()
	{
		return ArrayHelper::map(static::find()->all(), 'id', 'name');
	}
}
