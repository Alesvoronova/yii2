<?php

namespace app\modules\gym\models\search;

use app\modules\user\models\User;
use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\modules\gym\models\Gym;

/**
 * GymSearch represents the model behind the search form of `app\modules\gym\models\Gym`.
 */
class GymSearch extends Gym
{
	
	public $search;
	
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id', 'zip', 'manager_id', 'created_at', 'updated_at'], 'integer'],
            [['name', 'address', 'city', 'state', 'phone', 'slug', 'auth', 'search'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        return Model::scenarios();
    }
	
	/**
	 * {@inheritdoc}
	 */
	public function behaviors()
	{
		return [];
	}

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Gym::find();
	
	    if (!Yii::$app->user->can(User::ROLE_ADMIN)) {
		
		    $query->andWhere(['manager_id' => Yii::$app->user->id]);
	    }
        

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            return $dataProvider;
        }
        
        $query->andFilterWhere([
            'id' => $this->id,
            'zip' => $this->zip,
            'manager_id' => $this->manager_id,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
        ]);

        $query->andFilterWhere(['ilike', 'name', $this->name])
            ->andFilterWhere(['ilike', 'address', $this->address])
            ->andFilterWhere(['ilike', 'city', $this->city])
            ->andFilterWhere(['ilike', 'state', $this->state])
            ->andFilterWhere(['ilike', 'phone', $this->phone])
            ->andFilterWhere(['ilike', 'slug', $this->slug])
            ->andFilterWhere(['ilike', 'auth', $this->auth]);

        return $dataProvider;
    }
}
