<?php

use yii\helpers\Html;
use kartik\form\ActiveForm;
use yii\widgets\MaskedInput;

/* @var $this yii\web\View */
/* @var $model app\modules\gym\models\Gym */
/* @var $form yii\widgets\ActiveForm */
?>

<?php $form = ActiveForm::begin(); ?>

<div class="card">
	<div class="card-body">
		<div class="row">
			<div class="col-md-4">
				
				<?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>
				
				<?= $form->field($model, 'address')->textInput(['maxlength' => true]) ?>
			</div>
			<div class="col-md-4">
				
				<?= $form->field($model, 'city')->textInput(['maxlength' => true]) ?>
				
				<?= $form->field($model, 'state')->textInput(['maxlength' => true]) ?>
			</div>
			<div class="col-md-4">
				
				<?= $form->field($model, 'zip')->widget(MaskedInput::class, ['mask' => '99999', 'options' => ['placeholder' => '_____']])  ?>
				
				<?= $form->field($model, 'phone')->widget(MaskedInput::class, ['mask' => '999-999-9999', 'options' => ['placeholder' => '___-___-____']]) ?>
			</div>
		</div>
	</div>
</div>

<div class="col-lg-12">
	<?= Html::submitButton('Save', ['class' => 'btn btn-primary']) ?>
</div>

<?php ActiveForm::end(); ?>
