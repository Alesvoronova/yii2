<?php

/* @var $this yii\web\View */
/* @var $model app\modules\gym\models\Gym */

$this->title = 'Update Gym';
$this->params['breadcrumbs'][] = ['label' => 'Gyms', 'url' => ['index']];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="gym-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
