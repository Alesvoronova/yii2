<?php

/* @var $this yii\web\View */
/* @var $model app\modules\gym\models\Gym */

$this->title = 'Create Gym';
$this->params['breadcrumbs'][] = ['label' => 'Gyms', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="gym-create">
	
    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
