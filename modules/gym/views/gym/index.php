<?php

use yii\helpers\Html;
use kartik\grid\GridView;
use app\components\AdminGrid;
use kartik\grid\ActionColumn;

/* @var $this yii\web\View */
/* @var $searchModel app\modules\gym\models\search\GymSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Gyms';
$this->params['breadcrumbs'][] = $this->title;
?>

<?= AdminGrid::widget([
	'title'        => 'Gym',
	'extraSearch'  => $this->render('_search', ['model' => $searchModel]),
	'dataProvider' => $dataProvider,
	'filterModel'  => $searchModel,
	'createButton' => '',
	'columns'      => [
		AdminGrid::COLUMN_CHECKBOX,
		'name',
		'address',
		'city',
		'state',
		'zip',
		'phone',
		'created_at:date',
		'updated_at:date',
		[
			'class'         => ActionColumn::class,
			'template'      => '<div class="btn-group btn-group-sm">{update}{delete}</div>',
			'updateOptions' => [
				'class' => 'btn btn-outline-primary',
			],
			'deleteOptions' => [
				'class' => 'btn btn-outline-dark',
			],
			'header'        => Html::a('Clear', ['index'], ['class' => 'btn btn-outline-primary']),
			'vAlign'        => GridView::ALIGN_CENTER,
		],
	],
]); ?>
