<?php

namespace app\modules\schedule\controllers;

use app\modules\user\models\User;
use Yii;
use yii\filters\VerbFilter;
use yii\helpers\ArrayHelper;
use app\modules\gym\models\Gym;
use yii\web\NotFoundHttpException;
use app\modules\group\models\Group;
use app\modules\schedule\models\Schedule;
use app\components\controllers\BackController;
use app\modules\schedule\models\search\ScheduleSearch;

/**
 * ScheduleController implements the CRUD actions for Schedule model.
 */
class ScheduleController extends BackController
{
	
	/**
	 * {@inheritdoc}
	 */
	public function behaviors()
	{
		return [
			'verbs' => [
				'class'   => VerbFilter::className(),
				'actions' => [
					'delete' => ['POST'],
				],
			],
		];
	}
	
	/**
	 * Lists all Schedule models.
	 * @return mixed
	 */
	public function actionIndex()
	{
		$searchModel = new ScheduleSearch();
		$dataProvider = $searchModel->search(Yii::$app->request->queryParams);
		
		return $this->render('index', [
			'searchModel'  => $searchModel,
			'dataProvider' => $dataProvider,
		]);
	}
	
	/**
	 * Displays a single Schedule model.
	 *
	 * @param integer $id
	 *
	 * @return mixed
	 * @throws NotFoundHttpException if the model cannot be found
	 */
	public function actionView($id)
	{
		return $this->render('view', [
			'model' => $this->findModel($id),
		]);
	}
	
	/**
	 * Creates a new Schedule model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 * @return mixed
	 */
	public function actionCreate()
	{
		$model = new Schedule();
		$gym = Gym::findOne(['manager_id' => Yii::$app->user->id]);
		$group = ArrayHelper::map(Group::find()->where(['gym_id' => $gym->id])->all(), 'id', 'name');
		
		if($model->load(Yii::$app->request->post())) {
			$model->date = strtotime($model->date);
			$model->save();
			
			return $this->redirect(['index']);
		}
		
		return $this->render('create', [
			'model' => $model,
			'group' => $group,
		]);
	}
	
	/**
	 * Updates an existing Schedule model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 *
	 * @param integer $id
	 *
	 * @return mixed
	 * @throws NotFoundHttpException if the model cannot be found
	 */
	public function actionUpdate($id)
	{
		$model = $this->findModel($id);
		$gym = Gym::findOne(['manager_id' => Yii::$app->user->id]);
		$group = ArrayHelper::map(Group::find()->where(['gym_id' => $gym->id])->all(), 'id', 'name');
		
		if($model->load(Yii::$app->request->post())) {
			$model->date = strtotime($model->date);
			$model->save();
			
			return $this->redirect(['index']);
		}
		
		return $this->render('update', [
			'model' => $model,
			'group' => $group,
		]);
	}
	
	/**
	 * Deletes an existing Schedule model.
	 * If deletion is successful, the browser will be redirected to the 'index' page.
	 *
	 * @param integer $id
	 *
	 * @return mixed
	 * @throws NotFoundHttpException if the model cannot be found
	 */
	public function actionDelete($id)
	{
		$this->findModel($id)->delete();
		
		return $this->redirect(['index']);
	}
	
	/**
	 * Finds the Schedule model based on its primary key value.
	 * If the model is not found, a 404 HTTP exception will be thrown.
	 *
	 * @param integer $id
	 *
	 * @return Schedule the loaded model
	 * @throws NotFoundHttpException if the model cannot be found
	 */
	protected function findModel($id)
	{
		if(($model = Schedule::findOne($id)) !== null) {
			return $model;
		}
		
		throw new NotFoundHttpException('The requested page does not exist.');
	}
	
	public function actionDate($start, $end)
	{
		Yii::$app->response->format = 'json';
		
		$return = [];
		
		$color = function($key)
		{
			return !$key ? '#007bff' : '#6c757d';
		};
		
		$schedules = Schedule::find()
		                     ->joinWith(['group.gym'])
		                     ->where(['between', 'date', strtotime($start), strtotime($end)])
		                     ->asArray();
		
		if (!Yii::$app->user->can(User::ROLE_ADMIN)) {
		
			$schedules->andWhere(['gym.id' => Yii::$app->user->identity->gym_id]);
			if(!Yii::$app->user->can(User::ROLE_MANAGER) && !Yii::$app->user->can(User::ROLE_TRAINER) ) {
				$schedules->joinWith(['group.groupUsers']);
				$schedules->andWhere(['group_user_relative.user_id' => Yii::$app->user->id]);
			}
		}
		
		foreach($schedules->all() as $schedule) {
			$return[] = [
				'id'    => $schedule['id'],
				'title' => $schedule['group']['name'],
				'start' => date('Y-m-d H:i', $schedule['date']),
				'end'   => date('Y-m-d H:i', $schedule['date']),
				//'backgroundColor' => $color($shedule['send_status']),
				//'borderColor'     => $color($shedule['send_status']),
			];
		}
		
		return $return;
		
	}
}
