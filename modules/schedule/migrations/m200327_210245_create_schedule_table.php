<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%schedule}}`.
 */
class m200327_210245_create_schedule_table extends Migration
{
	
	public $table = '{{%schedule}}';
	
	/**
	 * {@inheritdoc}
	 */
	public function safeUp()
	{
		$this->createTable($this->table, [
			'id'       => $this->primaryKey(),
			'group_id' => $this->integer(),
			'date'     => $this->integer(),
		]);
		
		$this->addForeignKey(
			'fk-group-schedule',
			$this->table,
			'group_id',
			'group',
			'id',
			'SET NULL',
			'NO ACTION'
		);
		
	}
	
	/**
	 * {@inheritdoc}
	 */
	public function safeDown()
	{
		$this->dropForeignKey(
			'fk-group-schedule',
			$this->table
		);
		
		$this->dropTable($this->table);
	}
}
