<?php

namespace app\modules\schedule\models;

use app\modules\group\models\Group;
use Yii;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "schedule".
 *
 * @property int $id
 * @property int|null $group_id
 * @property int|null $date
 *
 * @property Group $group
 */
class Schedule extends \yii\db\ActiveRecord
{
	/**
	 * {@inheritdoc}
	 */
	public static function tableName()
	{
		return 'schedule';
	}
	
	/**
	 * {@inheritdoc}
	 */
	public function rules()
	{
		return [
			[['group_id', 'date'], 'default', 'value' => null],
			[['group_id'], 'integer'],
			[['date'], 'safe'],
			[['group_id'], 'exist', 'skipOnError' => true, 'targetClass' => Group::class, 'targetAttribute' => ['group_id' => 'id']],
		];
	}
	
	/**
	 * {@inheritdoc}
	 */
	public function attributeLabels()
	{
		return [
			'id' => 'ID',
			'group_id' => 'Group ID',
			'date' => 'Date',
		];
	}
	
	/**
	 * Gets query for [[Group]].
	 *
	 * @return \yii\db\ActiveQuery
	 */
	public function getGroup()
	{
		return $this->hasOne(Group::class, ['id' => 'group_id']);
	}
	
	/**
	 * Get Items ['ID'=> 'NAME', ..]
	 *
	 * @return array
	 */
	public static function items()
	{
		return ArrayHelper::map(static::find()->all(), 'id', 'name');
	}
}