<?php

namespace app\modules\schedule\models\search;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\modules\user\models\User;
use app\modules\schedule\models\Schedule;

/**
 * ScheduleSearch represents the model behind the search form of `app\modules\schedule\models\Schedule`.
 */
class ScheduleSearch extends Schedule
{
	
	public $search;
	
	/**
	 * {@inheritdoc}
	 */
	public function rules()
	{
		return [
			[['id', 'group_id', 'date'], 'integer'],
			[['search'], 'safe'],
		];
	}
	
	/**
	 * {@inheritdoc}
	 */
	public function scenarios()
	{
		return Model::scenarios();
	}
	
	/**
	 * Creates data provider instance with search query applied
	 *
	 * @param array $params
	 *
	 * @return ActiveDataProvider
	 */
	public function search($params)
	{
		$query = Schedule::find()->joinWith(['group']);
		
		if(!Yii::$app->user->can(User::ROLE_ADMIN)) {
			$query->where(['group.gym_id' => Yii::$app->user->identity->gym_id]);
			if(!Yii::$app->user->can(User::ROLE_MANAGER) && !Yii::$app->user->can(User::ROLE_TRAINER)) {
				$query->joinWith(['group.groupUsers']);
				$query->andWhere(['group_user_relative.user_id' => Yii::$app->user->id]);
			}
		}
		
		$dataProvider = new ActiveDataProvider([
			'query' => $query,
		]);
		
		$this->load($params);
		
		if(!$this->validate()) {
			return $dataProvider;
		}
		
		$query->andFilterWhere([
			'id'       => $this->id,
			'group_id' => $this->group_id,
			'date'     => $this->date,
		]);
		
		return $dataProvider;
	}
}
