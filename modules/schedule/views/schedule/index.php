<?php

use yii\helpers\Html;
use kartik\grid\ActionColumn;
use app\components\AdminGrid;
use app\modules\user\models\User;

/* @var $this yii\web\View */
/* @var $searchModel app\modules\schedule\models\search\ScheduleSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Schedules';
$this->params['breadcrumbs'][] = $this->title;
?>

<?= AdminGrid::widget([
	'title'        => 'Schedules',
	'extraSearch'  => $this->render('_search', ['model' => $searchModel]),
	'dataProvider' => $dataProvider,
	'filterModel'  => $searchModel,
	'createButton' => !Yii::$app->user->can(User::ROLE_ADMIN) && !Yii::$app->user->can(User::ROLE_MANAGER) ? false : true,
	
	'columns' => [
		AdminGrid::COLUMN_CHECKBOX,
		[
			'attribute' => 'group_id',
			//'filter'    => $group,
			'label'     => 'Group Name',
			'value'     => function($model)
			{
				return $model->group->name;
			},
		],
		'date:date',
		[
			'class'          => ActionColumn::class,
			'template'       => '<div class="btn-group btn-group-sm">{update}{delete}</div>',
			'visibleButtons' => [
				'update' => Yii::$app->user->can(User::ROLE_MANAGER),
				'delete' => Yii::$app->user->can(User::ROLE_MANAGER),
			],
			
			'updateOptions' => [
				'class' => 'btn btn-outline-primary',
			],
			'deleteOptions' => [
				'class' => 'btn btn-outline-dark',
			],
			'header'        => Html::a('Clear', ['index'], ['class' => 'btn btn-outline-primary']),
			'vAlign'        => \kartik\grid\GridView::ALIGN_CENTER,
		],
	],
]); ?>
