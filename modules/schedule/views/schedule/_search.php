<?php

use app\modules\gym\models\search\GymSearch;
use app\modules\user\models\User;
use kartik\form\ActiveField;
use yii\helpers\Html;
use kartik\form\ActiveForm;
use yii\web\View;

/* @var $this yii\web\View */
/* @var $model app\modules\gym\models\search\GymSearch */
/* @var $form \yii\widgets\ActiveForm */
?>


<?php $form = ActiveForm::begin([
		'action'  => ['index'],
		'method'  => 'get',
		'options' => [
			'data-pjax' => 1,
		],
	]
); ?>

<?= $form->field($model, 'search', [
	'addon' => [
		'append' => [
			'content'  => Html::submitButton('<i class="fas fa-search"></i>', ['class' => 'btn btn-sm btn-light']),
			'asButton' => true,
		],
	],
])->label(false) ?>

<?php ActiveForm::end(); ?>
