<?php

/* @var $this yii\web\View */
/* @var $model app\modules\schedule\models\Schedule */
/* @var $group app\modules\group\models\Group */

$this->title = 'Update Schedule: ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Schedules', 'url' => ['index']];
$this->params['breadcrumbs'][] = 'Update';

use app\modules\group\models\Group;
use app\modules\schedule\models\Schedule;
use yii\web\View; ?>
<?= $this->render('_form', [
	'model' => $model,
	'group' => $group,
]) ?>
