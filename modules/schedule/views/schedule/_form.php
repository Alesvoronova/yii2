<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\datetime\DateTimePicker;

/* @var $this yii\web\View */
/* @var $model app\modules\schedule\models\Schedule */
/* @var $form yii\widgets\ActiveForm */
/* @var $group [] app\modules\group\models\Group */

?>

<div class="schedule-form">
	
	<?php $form = ActiveForm::begin(); ?>
	
	<?= $form->field($model, 'group_id')->dropDownList($group, ['prompt' => 'Select Group']) ?>
	
	<?= $form->field($model, 'date')->widget(DateTimePicker::class, [
		'options'       => [
			'value' => $model->isNewRecord ? '' : Yii::$app->formatter->asDateTime($model->date),
		],
		'pluginOptions' => [
			'autoclose' => true,
			'format'    => 'mm/dd/yyyy hh:ii',
		],
	]) ?>

	<div class="form-group">
		<?= Html::submitButton('Save', ['class' => 'btn btn-primary']) ?>
	</div>
	
	<?php ActiveForm::end(); ?>

</div>
