<?php

/* @var $this yii\web\View */
/* @var $model app\modules\schedule\models\Schedule */
/* @var $group app\modules\group\models\Group */

$this->title = 'Create Schedule';
$this->params['breadcrumbs'][] = ['label' => 'Schedules', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;

use app\modules\group\models\Group;
use yii\web\View; ?>

<?= $this->render('_form', [
	'model' => $model,
	'group' => $group,
]) ?>
