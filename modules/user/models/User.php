<?php

namespace app\modules\user\models;

use app\modules\group\models\GroupUserRelative;
use app\modules\gym\models\Gym;
use Yii;
use yii\db\ActiveRecord;
use yii\helpers\ArrayHelper;
use yii\web\IdentityInterface;
use app\modules\admin\models\Theme;
use yii\behaviors\TimestampBehavior;

/**
 * This is the model class for table "user".
 *
 * @property int         $id
 * @property int         $last_login_at
 * @property int         $created_at
 * @property int         $gym_id
 * @property string      $email
 * @property string|null $password
 * @property bool        $blocked
 * @property bool        $confirmed
 * @property string|null $auth_key
 * @property string      $role
 * @property string      $first_name
 * @property string|null $last_name
 * @property string|null $phone
 * @property string|null $address
 * @property string|null $city
 * @property string|null $state
 * @property int|null    $zip
 * @property string|null $bio
 *
 * @property Theme       $theme
 *
 */
class User extends ActiveRecord implements IdentityInterface
{

    const ROLE_USER    = 'user';
    const ROLE_TRAINER = 'trainer';
    const ROLE_ADMIN   = 'admin';
    const ROLE_MANAGER = 'manager';

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'user';
    }

    public static function findIdentity($id)
    {
        return static::find()->where(['id' => $id])->with(['theme'])->one();
    }

    public static function findIdentityByAccessToken($token, $type = null)
    {
        return static::findOne(['auth_key' => $token]);
    }

    public function validateAuthKey($authKey)
    {
        return $this->authKey === $authKey;
    }

    public function getId()
    {
        return $this->id;
    }

    public function getAuthKey()
    {
        return $this->auth_key;
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['email', 'first_name'], 'required'],
            [['blocked', 'confirmed'], 'boolean'],
            [['zip'], 'default', 'value' => null],
            [['zip', 'gym_id', 'created_at', 'last_login_at', 'updated_at'], 'integer'],
            [['bio'], 'safe'],
            [['email', 'password', 'auth_key', 'role', 'first_name', 'last_name', 'phone', 'address', 'city', 'state'], 'string', 'max' => 255],
            [['email'], 'unique'],
        ];
    }

    public function behaviors()
    {
        return [
            [
                'class' => TimestampBehavior::class,
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id'            => '#',
            'email'         => 'Email',
            'password'      => 'Password',
            'blocked'       => 'Blocked',
            'confirmed'     => 'Confirmed',
            'auth_key'      => 'Auth Key',
            'role'          => 'Role',
            'first_name'    => 'First Name',
            'last_name'     => 'Last Name',
            'phone'         => 'Phone',
            'address'       => 'Address',
            'city'          => 'City',
            'state'         => 'State',
            'zip'           => 'Zip',
            'bio'           => 'Bio',
            'created_at'    => 'Registered At',
            'updated_at'    => 'Updated At',
            'last_login_at' => 'Last Login',
        ];
    }

    /**
     * Generates New User password Hash and Auth Key
     *
     * @param bool $insert
     *
     * @return bool
     * @throws \yii\base\Exception
     */
    public function beforeSave($insert)
    {
        if($insert) {
            $this->auth_key = Yii::$app->security->generateRandomString();
            $this->password = Yii::$app->security->generatePasswordHash($this->password);
        }

        return parent::beforeSave($insert);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTheme()
    {
        return $this->hasOne(Theme::class, ['user_id' => 'id']);
    }

    public function getFullName()
    {
        return $this->first_name . ' ' . $this->last_name;
    }

    /**
     * Get Items ['ID'=> 'NAME', ..]
     *
     * @return array
     */
    public static function items()
    {
        return ArrayHelper::map(static::find()->all(), 'id', 'fullName');
    }
	
	/**
	 * @return \yii\db\ActiveQuery
	 */
	public function getGroupUserRelatives()
	{
		return $this->hasMany(GroupUserRelative::class, ['user_id' => 'id']);
	}

	public function getGym()
	{
		return $this->hasOne(Gym::class, ['id' => 'gym_id']);
	}
}
