<?php

namespace app\modules\user\models;

use Yii;
use yii\base\Model;
use app\modules\gym\models\Gym;
use app\modules\admin\models\Theme;

/**
 *
 * @property User|null $user This property is read-only.
 *
 */
class SignUp extends Model
{

    public $email;

    public $password;

    public $passwordConfirm;

    public $company;

    public $first_name;

    public $rememberMe;

    /**
     * @return array the validation rules.
     */
    public function rules()
    {
        return [
            [['email', 'password', 'passwordConfirm', 'company', 'first_name'], 'required'],
            [['email'], 'email'],
            [['first_name'], 'string'],
            [['email'], 'unique', 'targetClass' => User::class, 'targetAttribute' => 'email'],
            [['company'], 'unique', 'targetClass' => Gym::class, 'targetAttribute' => 'name'],
            [['passwordConfirm'], 'compare', 'compareAttribute' => 'password'],
            ['rememberMe', 'boolean'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'email'      => 'Email',
            'first_name' => 'Your Name',
            'password'   => 'Password',
            'company'    => 'Company',
        ];
    }

    /**
     * Logs in a user using the provided username and password.
     * @return bool whether the user is logged in successfully
     */
    public function signUp()
    {
        if(!$this->validate()) {
            return false;
        }

        $user = new User();

        /**
         * Overload attributes
         */
        $user->attributes = $this->attributes;

        if(!$user->save()) {
            return false;
        }

        /**
         * Create GYM
         */
        $gym = new Gym;
        $gym->name = $this->company;
        $gym->manager_id = $user->id;
	    $gym->auth = Yii::$app->security->generateRandomString();
        $gym->save();

        /**
         * Create THEME for User
         */
        $theme = new Theme;
        $theme->user_id = $user->id;
        $theme->save();

        /**
         * Attach GYM to User
         */
        $user->updateAttributes(['gym_id' => $gym->id]);

        $user->touch('last_login_at');

        return Yii::$app->user->login($user, $this->rememberMe ? 3600 * 24 * 30 : 0);
    }
}
