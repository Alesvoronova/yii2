<?php

namespace app\modules\user\models\search;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\modules\user\models\User;

/**
 * UserSearch represents the model behind the search form of `app\modules\user\models\User`.
 */
class UserSearch extends User
{
	
	public $search;
	
	/**
	 * {@inheritdoc}
	 */
	public function rules()
	{
		return [
			[['id', 'gym_id', 'zip', 'created_at', 'updated_at', 'last_login_at'], 'integer'],
			[['email', 'password', 'auth_key', 'gym_id', 'role', 'first_name', 'last_name', 'phone', 'address', 'city', 'state', 'bio', 'search'], 'safe'],
			[['blocked', 'confirmed'], 'boolean'],
		];
	}
	
	/**
	 * {@inheritdoc}
	 */
	public function scenarios()
	{
		return Model::scenarios();
	}
	
	/**
	 * Creates data provider instance with search query applied
	 *
	 * @param array $params
	 *
	 * @return ActiveDataProvider
	 */
	public function search($params)
	{
		$query = User::find();
		
		if (!Yii::$app->user->can(User::ROLE_ADMIN)) {
			
			$query->andWhere(['gym_id' => Yii::$app->user->identity->gym_id]);
		}
		
		
		$dataProvider = new ActiveDataProvider([
			'query' => $query,
		]);
		
		$this->load($params);
		
		if(!$this->validate()) {
			return $dataProvider;
		}
		
		$query->andFilterWhere([
			'id'        => $this->id,
			'gym_id'    => $this->gym_id,
			'blocked'   => $this->blocked,
			'confirmed' => $this->confirmed,
			'zip'       => $this->zip,
		]);
		
		$query->andFilterWhere(['ilike', 'email', $this->email])
		      ->andFilterWhere(['ilike', 'password', $this->password])
		      ->andFilterWhere(['ilike', 'auth_key', $this->auth_key])
		      ->andFilterWhere(['ilike', 'role', $this->role])
		      ->andFilterWhere(['ilike', 'first_name', $this->first_name])
		      ->andFilterWhere(['ilike', 'last_name', $this->last_name])
		      ->andFilterWhere(['ilike', 'phone', $this->phone])
		      ->andFilterWhere(['ilike', 'address', $this->address])
		      ->andFilterWhere(['ilike', 'city', $this->city])
		      ->andFilterWhere(['ilike', 'state', $this->state])
		      ->andFilterWhere(['ilike', 'bio', $this->bio]);
		
		return $dataProvider;
	}
}
