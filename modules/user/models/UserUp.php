<?php

namespace app\modules\user\models;

use Yii;
use yii\base\Model;
use app\modules\admin\models\Theme;

/**
 *
 * @property User|null $user This property is read-only.
 *
 */
class UserUp extends Model
{
	
	public $email;
	
	public $password;
	
	public $passwordConfirm;
	
	public $first_name;
	
	public $last_name;
	
	public $gym_id;
	
	public $phone;
	
	public $rememberMe;
	
	/**
	 * @return array the validation rules.
	 */
	public function rules()
	{
		return [
			[['email', 'password', 'passwordConfirm', 'first_name', 'gym_id'], 'required'],
			[['email'], 'email'],
			[['gym_id'], 'integer'],
			[['first_name', 'last_name', 'phone'], 'string'],
			[['email'], 'unique', 'targetClass' => User::class, 'targetAttribute' => 'email'],
			[['passwordConfirm'], 'compare', 'compareAttribute' => 'password'],
			['rememberMe', 'boolean'],
		];
	}
	
	/**
	 * {@inheritdoc}
	 */
	public function attributeLabels()
	{
		return [
			'email'      => 'Email',
			'first_name' => 'Your First Name',
			'last_name'  => 'Your Last Name',
			'gym_id'     => 'Gym Name',
			'phone'      => 'Phone',
			'password'   => 'Password',
		];
	}
	
	/**
	 * Logs in a user using the provided username and password.
	 * @return bool whether the user is logged in successfully
	 */
	public function signUp()
	{
		if(!$this->validate()) {
			return false;
		}
		
		$user = new User();
		
		/**
		 * Overload attributes
		 */
		$user->attributes = $this->attributes;
		
		$user->role = 'user';
		
		if(!$user->save()) {
			return false;
		}
		
		/**
		 * Create THEME for User
		 */
		$theme = new Theme;
		$theme->user_id = $user->id;
		$theme->save();
		
		/**
		 * Attach GYM to User
		 */
		
		$user->touch('last_login_at');
		
		return Yii::$app->user->login($user, $this->rememberMe ? 3600 * 24 * 30 : 0);
	}
}
