<?php

use app\components\AdminGrid;
use kartik\grid\ActionColumn;
use kartik\grid\BooleanColumn;
use kartik\grid\GridView;
use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $searchModel app\modules\user\models\search\UserSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Users';
$this->params['breadcrumbs'][] = $this->title;
?>

<?= AdminGrid::widget([
	'title'        => 'Users',
	'extraSearch'  => $this->render('_search', ['model' => $searchModel]),
	'dataProvider' => $dataProvider,
	'filterModel'  => $searchModel,
	'createButton' => '',
	'columns'      => [
		AdminGrid::COLUMN_CHECKBOX,
		'email:email',
		'first_name',
		'last_name',
		'role',
		[
			'class'     => BooleanColumn::class,
			'attribute' => 'blocked',
		],
		[
			'class'     => BooleanColumn::class,
			'attribute' => 'confirmed',
		],
		'created_at:date',
		'last_login_at:datetime',
		[
			'class'         => ActionColumn::class,
			'template'      => '<div class="btn-group btn-group-sm">{delete}</div>',
			'deleteOptions' => [
				'class' => 'btn btn-outline-dark',
			],
			'header'        => Html::a('Clear', ['index'], ['class' => 'btn btn-outline-primary']),
			'vAlign'        => GridView::ALIGN_CENTER,
		],
	],
]); ?>


