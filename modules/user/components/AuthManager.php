<?php

/**
 * @link      http://industrialax.com/
 * @email     xristmas365@gmail.com
 * @author    isteil
 * @copyright Copyright (c) 2020 INDUSTRIALAX SOLUTIONS LLC
 * @license   https://industrialax.com/license
 */

namespace app\modules\user\components;

use Yii;
use yii\base\Component;
use yii\rbac\CheckAccessInterface;

/**
 * Class AuthManager
 * @package app\components
 */
class AuthManager extends Component implements CheckAccessInterface
{

    public $items     = [];

    public $itemsFile = '@app/config/roles.php';

    public function init()
    {
        $this->items = require_once Yii::getAlias($this->itemsFile);
        parent::init();
    }

    /**
     * @param int|string $userId
     * @param string     $permissionName
     * @param array      $params
     *
     * @return bool
     */
    public function checkAccess($userId, $permissionName, $params = [])
    {
        if(Yii::$app->user->isGuest) {
            return false;
        }

        if(key_exists($permissionName, $this->items)) {
            return $this->checkRoleAccess(Yii::$app->user->identity->role, $permissionName);
        }

        return false;

    }

    protected function checkRoleAccess($role, $permissionName)
    {
        if($role === $permissionName) {
            return true;
        }

        if(!key_exists('items', $this->items[$role])) {
            return false;
        }

        foreach($this->items[$role]['items'] as $child) {
            if($this->checkRoleAccess($child, $permissionName)) {
                return true;
            }
        }

        return false;

    }

    /**
     * Get role list
     * @return array
     */
    public function getRoles()
    {
        foreach($this->_items as $id => $role) {
            $roles[$id] = $role['description'];
        }

        if(!Yii::$app->user->can('developer'))
            unset($roles['developer']);

        return $roles;
    }

}
