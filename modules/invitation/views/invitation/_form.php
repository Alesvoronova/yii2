<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\modules\invitation\models\Invitation */
/* @var $form yii\widgets\ActiveForm */
/* @var $gym[] app\modules\gym\models\Gym */
?>
<?php $form = ActiveForm::begin(); ?>
<div class="card">
	<div class="card-body">
		<div class="row">
			<div class="col-md-6">
				
				<?= $form->field($model, 'email')->textInput(['maxlength' => true]) ?>
			</div>
		</div>
	</div>
</div>

<div class="col-lg-12">
	<?= Html::submitButton('Save', ['class' => 'btn btn-primary']) ?>
</div>

<?php ActiveForm::end(); ?>

