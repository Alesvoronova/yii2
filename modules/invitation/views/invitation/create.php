<?php

/* @var $this yii\web\View */
/* @var $model app\modules\invitation\models\Invitation */
/* @var $gym app\modules\gym\models\Gym */

$this->title = 'Create Invitation';
$this->params['breadcrumbs'][] = ['label' => 'Invitations', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;

?>

<div class="invitation-create">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
