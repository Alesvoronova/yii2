<?php

/* @var $this yii\web\View */
/* @var $model app\modules\invitation\models\Invitation */
/* @var $gym app\modules\gym\models\Gym */

$this->title = 'Update Invitation';
$this->params['breadcrumbs'][] = ['label' => 'Invitations', 'url' => ['index']];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="invitation-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
