<?php

use app\modules\user\models\User;
use yii\helpers\Html;
use kartik\grid\ActionColumn;
use app\components\AdminGrid;

/* @var $this yii\web\View */
/* @var $searchModel app\modules\invitation\models\search\InvitationSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */
/* @var $user app\modules\user\models\User */
/* @var $gym app\modules\gym\models\Gym */

$this->title = 'Invitations';
$this->params['breadcrumbs'][] = $this->title;
?>

<?= AdminGrid::widget([
	'title'        => 'Invitation',
	'extraSearch'  => $this->render('_search', ['model' => $searchModel]),
	'dataProvider' => $dataProvider,
	'filterModel'  => $searchModel,
	'createButton' => !Yii::$app->user->can(User::ROLE_ADMIN) && !Yii::$app->user->can(User::ROLE_MANAGER) ? false : true,
	'columns'      => [
		AdminGrid::COLUMN_CHECKBOX,
		'email:email',
		[
			'attribute' => 'gym_id',
			'filter'    => $gym,
			'label'     => 'Gym Name',
			'value'     => function($model)
			{
				return $model->gym->name;
			},
		],
		'created_at:date',
		[
			'attribute' => 'user_id',
			'filter'    => $user,
			'label'     => 'Manager',
			'value'     => function($model)
			{
				return $model->user->first_name . ' ' . $model->user->last_name;
			},
		],
		[
			'class'         => ActionColumn::class,
			'template'      => '<div class="btn-group btn-group-sm">{update}{delete}</div>',
			'updateOptions' => [
				'class' => 'btn btn-outline-primary',
			],
			'deleteOptions' => [
				'class' => 'btn btn-outline-dark',
			],
			'header'        => Html::a('Clear', ['index'], ['class' => 'btn btn-outline-primary']),
			'vAlign'        => \kartik\grid\GridView::ALIGN_CENTER,
		],
	],
]); ?>
