<?php

namespace app\modules\invitation\models;

use app\modules\gym\models\Gym;
use app\modules\user\models\User;
use yii\behaviors\BlameableBehavior;
use yii\behaviors\TimestampBehavior;

/**
 * This is the model class for table "invitation".
 *
 * @property int $id
 * @property string|null $email
 * @property int|null $gym_id
 * @property int|null $created_at
 * @property int|null $user_id
 *
 * @property Gym $gym
 */
class Invitation extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'invitation';
    }
	
	public function behaviors()
	{
		return [
			[
				'class'              => TimestampBehavior::class,
				'updatedAtAttribute' => false,
			],
			[
				'class'              => BlameableBehavior::class,
				'createdByAttribute' => 'user_id',
				'updatedByAttribute' => false,
			],
		];
	}

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['gym_id', 'created_at', 'user_id'], 'default', 'value' => null],
            [['gym_id', 'created_at', 'user_id'], 'integer'],
            [['email'], 'string', 'max' => 255],
            [['email'], 'email'],
            [['gym_id'], 'exist', 'skipOnError' => true, 'targetClass' => Gym::class, 'targetAttribute' => ['gym_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'email' => 'Email',
            'gym_id' => 'Gym Name',
            'created_at' => 'Created At',
            'user_id' => 'User',
        ];
    }

    /**
     * Gets query for [[Gym]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getGym()
    {
        return $this->hasOne(Gym::class, ['id' => 'gym_id']);
    }
	
	/**
	 * Gets query for [[Manager]].
	 *
	 * @return \yii\db\ActiveQuery
	 */
	public function getUser()
	{
		return $this->hasOne(User::class, ['id' => 'user_id']);
	}
}
