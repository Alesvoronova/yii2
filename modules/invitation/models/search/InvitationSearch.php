<?php

namespace app\modules\invitation\models\search;

use app\modules\user\models\User;
use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\modules\invitation\models\Invitation;

/**
 * InvitationSearch represents the model behind the search form of `app\modules\invitation\models\Invitation`.
 */
class InvitationSearch extends Invitation
{
	
	public $search;
	
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id', 'gym_id', 'created_at', 'user_id'], 'integer'],
            [['email', 'search'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Invitation::find()->joinWith(['user', 'gym']);
	
	    if (!Yii::$app->user->can(User::ROLE_ADMIN)) {
		
		    $query->andWhere(['user_id' => Yii::$app->user->id]);
	    }

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            return $dataProvider;
        }
        
        $query->andFilterWhere([
            'id' => $this->id,
            'gym_id' => $this->gym_id,
            'created_at' => $this->created_at,
            'user.id' => $this->user_id,
        ]);

        $query->andFilterWhere(['ilike', 'email', $this->email]);

        return $dataProvider;
    }
}
