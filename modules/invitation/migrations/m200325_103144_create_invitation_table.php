<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%invitation}}`.
 */
class m200325_103144_create_invitation_table extends Migration
{
	
	public $table = '{{%invitation}}';
	
	/**
	 * {@inheritdoc}
	 */
	public function safeUp()
	{
		$this->createTable($this->table, [
			'id'         => $this->primaryKey(),
			'email'      => $this->string(),
			'gym_id'     => $this->integer(),
			'created_at' => $this->integer(),
			'user_id'    => $this->integer(),
		]);
		
		$this->addForeignKey(
			'fk-gym-invitation_gym_id',
			$this->table,
			'gym_id',
			'gym',
			'id',
			'SET NULL',
			'NO ACTION'
		);
		
	}
	
	/**
	 * {@inheritdoc}
	 */
	public function safeDown()
	{
		$this->dropForeignKey(
			'fk-gym-invitation_gym_id',
			$this->table
		);
		
		$this->dropTable($this->table);
	}
}
