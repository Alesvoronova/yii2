<?php

namespace app\modules\invitation\controllers;

use Yii;
use yii\filters\VerbFilter;
use app\modules\gym\models\Gym;
use app\modules\user\models\User;
use yii\web\NotFoundHttpException;
use app\modules\invitation\models\Invitation;
use app\components\controllers\BackController;
use app\modules\invitation\models\search\InvitationSearch;

/**
 * InvitationController implements the CRUD actions for Invitation model.
 */
class InvitationController extends BackController
{
	
	/**
	 * {@inheritdoc}
	 */
	public function behaviors()
	{
		return [
			'verbs' => [
				'class'   => VerbFilter::className(),
				'actions' => [
					'delete' => ['POST'],
				],
			],
		];
	}
	
	/**
	 * Lists all Invitation models.
	 * @return mixed
	 */
	public function actionIndex()
	{
		$searchModel = new InvitationSearch();
		$dataProvider = $searchModel->search(Yii::$app->request->queryParams);
		$user = User::items();
		$gym = Gym::items();
		
		return $this->render('index', [
			'searchModel'  => $searchModel,
			'dataProvider' => $dataProvider,
			'user'         => $user,
			'gym'          => $gym,
		]);
	}
	
	///**
	// * Displays a single Invitation model.
	// * @param integer $id
	// * @return mixed
	// * @throws NotFoundHttpException if the model cannot be found
	// */
	//public function actionView($id)
	//{
	//    return $this->render('view', [
	//        'model' => $this->findModel($id),
	//    ]);
	//}
	
	/**
	 * Creates a new Invitation model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 * @return mixed
	 */
	public function actionCreate()
	{
		$model = new Invitation();
		
		if($model->load(Yii::$app->request->post())) {
			$gym = Gym::findOne(['manager_id' => Yii::$app->user->id]);
			$model->gym_id = $gym->id;
			
			$model->save();
			
			
			$mg = \Mailgun\Mailgun::create('2283b0c181a0a301fc86e7353ca566d8-19f318b0-f0ccc020');
			
			/**
			 * Trainer Notification
			 */
			$mg->messages()->send('5th-store.com', [
				'from'    => 'Fitness <fitness@5th-store.com>',
				'to'      => $model->email,
				'subject' => 'Gym:' . $gym->name,
				'html'    => 'You are invited to register: http://fitness.localhost/sign/trainer-up?auth=' . $gym->auth,
				//'h:X-Mailgun-Variables' => Json::encode([
				//	'url' => 'http://fitness.localhost/sign/trainer-up?auth=' . $auth->auth,
				//]),
			]);
			
			return $this->redirect(['index']);
		}
		
		return $this->render('create', [
			'model' => $model,
		]);
	}
	
	/**
	 * Updates an existing Invitation model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 *
	 * @param integer $id
	 *
	 * @return mixed
	 * @throws NotFoundHttpException if the model cannot be found
	 */
	public function actionUpdate($id)
	{
		$model = $this->findModel($id);
		
		if($model->load(Yii::$app->request->post())) {
			$gym = Gym::findOne(['manager_id' => Yii::$app->user->id]);
			$model->gym_id = $gym->id;
			
			$model->save();
			
			return $this->redirect(['index']);
		}
		
		return $this->render('update', [
			'model' => $model,
		]);
	}
	
	/**
	 * Deletes an existing Invitation model.
	 * If deletion is successful, the browser will be redirected to the 'index' page.
	 *
	 * @param integer $id
	 *
	 * @return mixed
	 * @throws NotFoundHttpException if the model cannot be found
	 */
	public function actionDelete($id)
	{
		$this->findModel($id)->delete();
		
		return $this->redirect(['index']);
	}
	
	/**
	 * Finds the Invitation model based on its primary key value.
	 * If the model is not found, a 404 HTTP exception will be thrown.
	 *
	 * @param integer $id
	 *
	 * @return Invitation the loaded model
	 * @throws NotFoundHttpException if the model cannot be found
	 */
	protected function findModel($id)
	{
		if(($model = Invitation::findOne($id)) !== null) {
			return $model;
		}
		
		throw new NotFoundHttpException('The requested page does not exist.');
	}
}
