<?php

namespace app\modules\invitation;

class Module extends \yii\base\Module
{
	
	public $adminUrl        = '/admin/dashboard/index';
	
	public $sessionDuration = 60 * 60 * 24 * 30; // 1 month
	
}
