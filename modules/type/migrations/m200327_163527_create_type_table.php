<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%type}}`.
 */
class m200327_163527_create_type_table extends Migration
{
	
	public $table = '{{%type}}';
	
	/**
	 * {@inheritdoc}
	 */
	public function safeUp()
	{
		$this->createTable($this->table, [
			'id'     => $this->primaryKey(),
			'name'   => $this->string(),
			'gym_id' => $this->integer(),
		]);
		
		$this->addForeignKey(
			'fk-gym-type_gym_id',
			$this->table,
			'gym_id',
			'gym',
			'id',
			'CASCADE',
			'NO ACTION'
		);
	}
	
	/**
	 * {@inheritdoc}
	 */
	public function safeDown()
	{
		$this->dropForeignKey(
			'fk-gym-type_gym_id',
			$this->table
		);
		
		$this->dropTable($this->table);
	}
}
