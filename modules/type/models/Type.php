<?php

namespace app\modules\type\models;

use app\modules\gym\models\Gym;
use Yii;

/**
 * This is the model class for table "type".
 *
 * @property int $id
 * @property string|null $name
 * @property int|null $gym_id
 *
 * @property Gym $gym
 */
class Type extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'type';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['gym_id'], 'default', 'value' => null],
            [['gym_id'], 'integer'],
            [['name'], 'string', 'max' => 255],
            [['gym_id'], 'exist', 'skipOnError' => true, 'targetClass' => Gym::class, 'targetAttribute' => ['gym_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
            'gym_id' => 'Gym ID',
        ];
    }

    /**
     * Gets query for [[Gym]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getGym()
    {
        return $this->hasOne(Gym::class, ['id' => 'gym_id']);
    }
}
