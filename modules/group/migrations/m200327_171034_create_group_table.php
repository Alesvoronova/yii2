<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%group}}`.
 */
class m200327_171034_create_group_table extends Migration
{
	
	public $table = '{{%group}}';
	
	/**
	 * {@inheritdoc}
	 */
	public function safeUp()
	{
		$this->createTable($this->table, [
			'id'      => $this->primaryKey(),
			'name'    => $this->string()->notNull()->unique(),
			'user_id' => $this->integer(),
			'gym_id'  => $this->integer(),
		]);
		
		$this->addForeignKey(
			'fk-user-group_user_id',
			$this->table,
			'user_id',
			'user',
			'id',
			'SET NULL',
			'NO ACTION'
		);
		
		$this->addForeignKey(
			'fk-gym-group_gym_id',
			$this->table,
			'gym_id',
			'gym',
			'id',
			'SET NULL',
			'NO ACTION'
		);
	}
	
	/**
	 * {@inheritdoc}
	 */
	public function safeDown()
	{
		$this->dropForeignKey(
			'fk-user-group_user_id',
			$this->table
		);
		
		$this->dropForeignKey(
			'fk-gym-group_gym_id',
			$this->table
		);
		
		$this->dropTable($this->table);
	}
}
