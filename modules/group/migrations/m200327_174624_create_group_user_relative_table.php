<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%group_user_relative}}`.
 */
class m200327_174624_create_group_user_relative_table extends Migration
{
	
	public $table = '{{%group_user_relative}}';
	
	/**
	 * {@inheritdoc}
	 */
	public function safeUp()
	{
		$this->createTable($this->table, [
			'group_id' => $this->integer(),
			'user_id'  => $this->integer(),
		]);
		
		$this->addForeignKey(
			'fk-user_relative-group',
			$this->table,
			'group_id',
			'group',
			'id',
			'CASCADE',
			'NO ACTION'
		);
		
		$this->addForeignKey(
			'fk-user_relative-user',
			$this->table,
			'user_id',
			'user',
			'id',
			'CASCADE',
			'NO ACTION'
		);
	}
	
	/**
	 * {@inheritdoc}
	 */
	public function safeDown()
	{
		$this->dropForeignKey(
			'fk-user_relative-group',
			$this->table
		);
		
		$this->dropForeignKey(
			'fk-user_relative-user',
			$this->table
		);
		
		$this->dropTable($this->table);
	}
}
