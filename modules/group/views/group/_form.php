<?php

use yii\helpers\Html;
use kartik\select2\Select2;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use app\modules\gym\models\Gym;

/* @var $this yii\web\View */
/* @var $model app\modules\group\models\Group */
/* @var $form yii\widgets\ActiveForm */
/* @var $coaches array */
/* @var $gym Gym */
?>

<div class="group-form">

    <?php $form = ActiveForm::begin(); ?>
	
	<?= $form->field($model, 'gym_id')->textInput(['value' => $gym->name, 'disabled' => true])->label('Gym') ?>
	
	<?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>
	
	<?= $form->field($model, 'user_id')->dropDownList($coaches, ['prompt' => 'Select Trainer ...'])->label('Trainer') ?>
	
	<?= $form->field($model, 'users')->widget(Select2::class, [
		'options'       => [
			'placeholder'  => 'Search users',
			'id'           => 'remote',
			'multiple'     => true,
			'autocomplete' => true,
			'value'        => $model->isNewRecord ? '' : ArrayHelper::getColumn($model->groupUsers, 'user.id'),
		],
		'data'          => $data,
		'pluginOptions' => [
			'allowClear'         => true,
		],
	])->hint('You can use multiple users') ?>

	<div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-primary']) ?>
    </div>
	
	<?php ActiveForm::end(); ?>

</div>
