<?php

/* @var $this yii\web\View */
/* @var $model app\modules\group\models\Group */
/* @var $gym app\modules\gym\models\Gym */
/* @var $coaches array */

$this->title = 'Update Group: ' . $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Groups', 'url' => ['index']];
$this->params['breadcrumbs'][] = 'Update';

?>

    <?= $this->render('_form', [
        'model' => $model,
        'coaches' => $coaches,
        'gym'     => $gym,
        'data'    => $data,

]) ?>

