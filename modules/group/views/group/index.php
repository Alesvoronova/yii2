<?php

use app\components\AdminGrid;
use app\modules\user\models\User;
use kartik\grid\ActionColumn;
use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $searchModel app\modules\group\models\search\GroupSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Groups';
$this->params['breadcrumbs'][] = $this->title;
?>
<?= AdminGrid::widget([
	'title'        => 'Group',
	'extraSearch'  => $this->render('_search', ['model' => $searchModel]),
	'dataProvider' => $dataProvider,
	'filterModel'  => $searchModel,
	'createButton' => !Yii::$app->user->can(User::ROLE_ADMIN) && !Yii::$app->user->can(User::ROLE_MANAGER) ? false : true,
	'columns'      => [
		AdminGrid::COLUMN_CHECKBOX,
		'name',
		[
			'attribute' => 'gym_id',
			//'filter'    => $gym,
			'label'     => 'Gym Name',
			'value'     => function($model)
			{
				return $model->gym->name;
			},
		],
		[
			'attribute' => 'user_id',
			//'filter'    => $user,
			'label'     => 'Manager',
			'value'     => function($model)
			{
				return $model->user->first_name . ' ' . $model->user->last_name;
			},
		],
		[
			'class'         => ActionColumn::class,
			'template'      => '<div class="btn-group btn-group-sm">{update}{delete}</div>',
			'updateOptions' => [
				'class' => 'btn btn-outline-primary',
			],
			'deleteOptions' => [
				'class' => 'btn btn-outline-dark',
			],
			'header'        => Html::a('Clear', ['index'], ['class' => 'btn btn-outline-primary']),
			'vAlign'        => \kartik\grid\GridView::ALIGN_CENTER,
		],
	],
]); ?>
