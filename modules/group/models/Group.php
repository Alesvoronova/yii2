<?php

namespace app\modules\group\models;

use app\modules\gym\models\Gym;
use app\modules\market\models\ProductTag;
use app\modules\user\models\User;
use Yii;

/**
 * This is the model class for table "group".
 *
 * @property int $id
 * @property string $name
 * @property int|null $user_id
 * @property int|null $gym_id
 *
 * @property Gym $gym
 * @property User $user
 */
class Group extends \yii\db\ActiveRecord
{
	
	public $users;
	
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'group';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['name'], 'required'],
            [['user_id', 'gym_id'], 'default', 'value' => null],
            [['user_id', 'gym_id'], 'integer'],
            [['name'], 'string', 'max' => 255],
            [['name'], 'unique'],
            [['users'], 'safe'],
            [['gym_id'], 'exist', 'skipOnError' => true, 'targetClass' => Gym::class, 'targetAttribute' => ['gym_id' => 'id']],
            [['user_id'], 'exist', 'skipOnError' => true, 'targetClass' => User::class, 'targetAttribute' => ['user_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
            'user_id' => 'User ID',
            'gym_id' => 'Gym ID',
        ];
    }

    /**
     * Gets query for [[Gym]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getGym()
    {
        return $this->hasOne(Gym::class, ['id' => 'gym_id']);
    }

    /**
     * Gets query for [[User]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::class, ['id' => 'user_id']);
    }
    
	
	public function getGroupUsers()
	{
		return $this->hasMany(GroupUserRelative::class, ['group_id' => 'id'])->with('user');
	}
}
