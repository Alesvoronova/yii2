<?php

namespace app\modules\group\models\search;

use app\modules\user\models\User;
use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\modules\group\models\Group;

/**
 * GroupSearch represents the model behind the search form of `app\modules\group\models\Group`.
 */
class GroupSearch extends Group
{
	
	public $search;
	
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id', 'user_id', 'gym_id'], 'integer'],
            [['name', 'search'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Group::find()->joinWith(['user', 'gym']);
	
	    if (!Yii::$app->user->can(User::ROLE_ADMIN)) {
		
		    $query->andWhere(['group.gym_id' => Yii::$app->user->identity->gym_id]);
	    }
	    
        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            return $dataProvider;
        }
        
        $query->andFilterWhere([
            'id' => $this->id,
            'group.user_id' => $this->user_id,
            'group.gym_id' => $this->gym_id,
        ]);

        $query->andFilterWhere(['ilike', 'name', $this->name]);

        return $dataProvider;
    }
}
