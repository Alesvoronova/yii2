<?php

namespace app\modules\group\controllers;

use Yii;
use yii\filters\VerbFilter;
use yii\helpers\ArrayHelper;
use app\modules\gym\models\Gym;
use app\modules\user\models\User;
use yii\web\NotFoundHttpException;
use app\modules\group\models\Group;
use app\components\controllers\BackController;
use app\modules\group\models\GroupUserRelative;
use app\modules\group\models\search\GroupSearch;

/**
 * GroupController implements the CRUD actions for Group model.
 */
class GroupController extends BackController
{
	
	/**
	 * {@inheritdoc}
	 */
	public function behaviors()
	{
		return [
			'verbs' => [
				'class'   => VerbFilter::className(),
				'actions' => [
					'delete' => ['POST'],
				],
			],
		];
	}
	
	/**
	 * Lists all Group models.
	 * @return mixed
	 */
	public function actionIndex()
	{
		$searchModel = new GroupSearch();
		$dataProvider = $searchModel->search(Yii::$app->request->queryParams);
		
		return $this->render('index', [
			'searchModel'  => $searchModel,
			'dataProvider' => $dataProvider,
		]);
	}
	
	/**
	 * Displays a single Group model.
	 *
	 * @param integer $id
	 *
	 * @return mixed
	 * @throws NotFoundHttpException if the model cannot be found
	 */
	public function actionView($id)
	{
		return $this->render('view', [
			'model' => $this->findModel($id),
		]);
	}
	
	/**
	 * Creates a new Group model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 * @return mixed
	 */
	public function actionCreate()
	{
		$model = new Group();
		$gym = Gym::findOne(['manager_id' => Yii::$app->user->id]);
		$coaches = ArrayHelper::map(User::find()
		                                ->where(['role' => User::ROLE_TRAINER])
		                                ->andWhere(['gym_id' => $gym->id])
		                                ->all(), 'id', 'fullname');
		
		$data = ArrayHelper::map(User::find()
		                             ->where(['gym_id' => Yii::$app->user->identity->gym_id, 'role' => User::ROLE_USER])
		                             ->all(), 'id', 'fullname');
		if(Yii::$app->request->post()) {
			$post = Yii::$app->request->post();
			$model->load($post);
			$model->gym_id = $gym->id;
			$model->save();
			$users = $post['Group']['users'];
			if(!empty($users)) {
				foreach($users as $user) {
					$groupUser = new GroupUserRelative;
					$groupUser->user_id = $user;
					$groupUser->group_id = $model->id;
					$groupUser->save();
				}
				
			}
			
			return $this->redirect(['index']);
		}
		
		return $this->render('create', [
			'model'   => $model,
			'coaches' => $coaches,
			'gym'     => $gym,
			'data'    => $data,
		]);
	}
	
	/**
	 * Updates an existing Group model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 *
	 * @param integer $id
	 *
	 * @return mixed
	 * @throws NotFoundHttpException if the model cannot be found
	 */
	public function actionUpdate($id)
	{
		$model = $this->findModel($id);
		$gym = Gym::findOne(['manager_id' => Yii::$app->user->id]);
		$coaches = ArrayHelper::map(User::find()
		                                ->where(['role' => User::ROLE_TRAINER])
		                                ->andWhere(['gym_id' => $gym->id])
		                                ->all(), 'id', 'fullname');
		$data = ArrayHelper::map(User::find()
		                             ->where(['gym_id' => Yii::$app->user->identity->gym_id, 'role' => User::ROLE_USER])
		                             ->all(), 'id', 'fullname');
		if(Yii::$app->request->post()) {
			$post = Yii::$app->request->post();
			$model->load($post);
			$model->gym_id = $gym->id;
			$model->save();
			$users = $post['Group']['users'];
			
			GroupUserRelative::deleteAll(['group_id' => $model->id]);
			foreach($users as $user) {
				$groupUser = new GroupUserRelative;
				$groupUser->user_id = $user;
				$groupUser->group_id = $model->id;
				$groupUser->save();
			}
			
			return $this->redirect(['index']);
		}
		
		return $this->render('update', [
			'model'   => $model,
			'coaches' => $coaches,
			'gym'     => $gym,
			'data'    => $data,
		]);
	}
	
	/**
	 * Deletes an existing Group model.
	 * If deletion is successful, the browser will be redirected to the 'index' page.
	 *
	 * @param integer $id
	 *
	 * @return mixed
	 * @throws NotFoundHttpException if the model cannot be found
	 */
	public function actionDelete($id)
	{
		$this->findModel($id)->delete();
		
		return $this->redirect(['index']);
	}
	
	/**
	 * Finds the Group model based on its primary key value.
	 * If the model is not found, a 404 HTTP exception will be thrown.
	 *
	 * @param integer $id
	 *
	 * @return Group the loaded model
	 * @throws NotFoundHttpException if the model cannot be found
	 */
	protected function findModel($id)
	{
		if(($model = Group::findOne($id)) !== null) {
			return $model;
		}
		
		throw new NotFoundHttpException('The requested page does not exist.');
	}
	
}
