<?php
/**
 * @link      http://industrialax.com/
 * @email     xristmas365@gmail.com
 * @author    isteil
 * @copyright Copyright (c) 2020 INDUSTRIALAX SOLUTIONS LLC
 * @license   https://industrialax.com/license
 */

use yii\helpers\Html;

?>
<div class="nk-sidebar">
	<div class="nk-nav-scroll">
		<ul class="metismenu" id="menu">
			<?php foreach($items as $item) : ?><?php if(key_exists('visible', $item) && !$item['visible'])
          continue;
          ?><?php if(isset($item['section'])) : ?>
          <li class="nav-label"><?= $item['section'] ?></li>
          <?php else : ?>
          <li class="mega-menu text-uppercase"><?= Html::a('<i class="' . $item['icon'] . '"></i><span class="nav-text">' . $item['label'] . '</span>', $item['url']) ?></li>
          <?php endif ?><?php endforeach ?>
		</ul>
	</div>
</div>

