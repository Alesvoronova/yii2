<?php

/**
 * @link      http://industrialax.com/
 * @email     xristmas365@gmail.com
 * @author    isteil
 * @copyright Copyright (c) 2020 INDUSTRIALAX SOLUTIONS LLC
 * @license   https://industrialax.com/license
 */

use app\modules\user\models\User;

return [
    [
        'label' => 'Dashboard',
        'icon'  => 'mdi mdi-diamond',
        'url'   => ['/admin/dashboard/index'],
    ],
    ['section' => 'System'],
    [
        'label'   => 'Users',
        'visible' => Yii::$app->user->can(User::ROLE_MANAGER),
        'icon'    => 'mdi mdi-account-multiple',
        'url'     => ['/user/default/index'],
    ],
    [
        'label'   => 'Gym',
        'visible' => Yii::$app->user->can(User::ROLE_MANAGER),
        'icon'    => 'mdi mdi-dumbbell',
        'url'     => ['/gym/gym/index'],
    ],
    [
        'label' => 'Invitation',
        'visible' => Yii::$app->user->can(User::ROLE_MANAGER),
        'icon'  => 'mdi mdi-email',
        'url'   => ['/invitation/invitation/index'],
    ],
	[
		'label' => 'Group',
		'visible' => Yii::$app->user->can(User::ROLE_MANAGER),
		'icon'  => 'mdi mdi-account-multiple',
		'url'   => ['/group/group/index'],
	],
	[
		'label' => 'Schedule',
		'visible' => Yii::$app->user->can(User::ROLE_USER),
		'icon'  => 'mdi mdi-account-multiple',
		'url'   => ['/schedule/schedule/index'],
	],
    [
        'label' => 'Settings',
        'visible' => Yii::$app->user->can(User::ROLE_ADMIN),
        'icon'  => 'fa fa-cogs',
        'url'   => ['/settings/view/index'],
    ],
    [
        'label' => 'Logs',
        'visible' => Yii::$app->user->can(User::ROLE_ADMIN),
        'icon'  => 'fa fa-file',
        'url'   => ['/system/log/index'],
    ],
];
