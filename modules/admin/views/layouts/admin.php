<?php

use yii\helpers\Html;
use yii\bootstrap4\Breadcrumbs;
use kartik\icons\FontAwesomeAsset;
use app\modules\admin\models\Theme;
use app\modules\admin\widgets\LeftMenu;
use app\modules\admin\assets\AdminAsset;

/**
 * @var Theme $theme
 */
$theme = Yii::$app->user->identity->theme;

AdminAsset::register($this);
FontAwesomeAsset::register($this);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
	<meta charset="<?= Yii::$app->charset ?>">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<?php $this->registerCsrfMetaTags() ?>
	<title><?= Html::encode($this->title) ?></title>
	<?php $this->head() ?>
</head>
<body
	data-sidebar-position="<?= $theme->sidebar_position ?>"
	data-theme-version="<?= $theme->version ?>"
	data-header-position="<?= $theme->header_position ?>"
	data-sidebar-style="<?= $theme->sidebar_style ?>"
	data-layout="<?= $theme->layout ?>"
	data-container="<?= $theme->container_layout ?>"
	data-nav-headerbg="<?= $theme->navheader_bg ?>"
	data-headerbg="<?= $theme->header_bg ?>"
	data-sibebarbg="<?= $theme->sidebar_bg ?>"
>
<?php $this->beginBody() ?>
<div id="preloader">
	<div class="loader">
		<svg class="circular" viewBox="25 25 50 50">
			<circle class="path" cx="50" cy="50" r="20" fill="none" stroke-width="3" stroke-miterlimit="10"/>
		</svg>
	</div>
</div>
<div id="main-wrapper">
	<?= $this->render('parts/_header') ?>
	<?= LeftMenu::widget() ?>
	<div class="content-body">
		<div class="container-fluid">
			<?php if($this->context->id !== 'dashboard') : ?>
				<div class="row page-titles">
					<div class="col p-md-0">
						<h4><?= $this->title ?? '' ?></h4>
					</div>
					<div class="col p-md-0">
						<?= Breadcrumbs::widget([
							'links'    => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
							'homeLink' => ['label' => 'Dashboard', 'url' => ['/admin/dashboard/index']],
						]) ?>
					</div>
				</div>
			<?php endif ?>
			<?= $content ?>
		</div>
	</div>
	<?= $this->render('parts/_footer') ?>
	<?= $this->render('parts/_right_sidebar') ?>
	<button id="to-top" class="to-top">
		<i class="fas fa-chevron-up"></i>
	</button>
</div>
<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>
