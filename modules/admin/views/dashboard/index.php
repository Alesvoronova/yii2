<?php

use app\assets\EventAsset;
use app\modules\admin\assets\DashboardAsset;

DashboardAsset::register($this);

EventAsset::register($this);

$this->title = 'Dashboard';

?>

<div class="row">
	<div class="col-md-6 bg-light">
		<div id="calendar"></div>
	</div>
</div>


