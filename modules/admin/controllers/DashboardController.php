<?php

namespace app\modules\admin\controllers;

use Yii;
use yii\web\Controller;
use yii\filters\AccessControl;
use app\modules\admin\models\Theme;

class DashboardController extends Controller
{
	
	public $layout = 'admin';
	
	public function behaviors()
	{
		return [
			'access' => [
				'class' => AccessControl::class,
				'rules' => [
					[
						'allow' => true,
						'roles' => ['@'],
					],
				
				],
			],
		];
	}
	
	/**
	 * Administration index page
	 */
	public function actionIndex()
	{
		return $this->render('index');
	}
	
	public function actionChangeTheme()
	{
		$key = Yii::$app->request->post('key');
		$value = Yii::$app->request->post('value');
		
		/**
		 * @var $theme Theme
		 */
		$theme = Yii::$app->user->identity->theme;
		
		return $theme->updateAttributes([$key => $value]);
		
	}
	
	
}
