<?php

use yii\helpers\Html;
use kartik\form\ActiveForm;
use app\modules\user\models\SignUp;
use yii\widgets\MaskedInput;

/* @var $this yii\web\View */

/* @var $form ActiveForm */

/* @var $model SignUp */

$this->title = 'Registration';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="row d-flex justify-content-center mt-5">
    <div class="col-md-6">
      <div class="jumbotron">

        <h1 class="text-center">Welcome</h1>

        <hr>
          <?php $form = ActiveForm::begin() ?>

          <?= $form->field($model, 'email') ?>
	      
          <?= $form->field($model, 'first_name') ?>
          
	      <?= $form->field($model, 'last_name') ?>
	      
	      <?= $form->field($model, 'phone')->widget(MaskedInput::class, ['mask' => '999-999-9999', 'options' => ['placeholder' => '___-___-____']])?>

          <?= $form->field($model, 'password')->passwordInput() ?>

          <?= $form->field($model, 'passwordConfirm')->passwordInput() ?>

          <?= $form->field($model, 'rememberMe')->checkbox() ?>

          <?= Html::submitButton('Sign Up', ['class' => 'btn btn-primary btn-block']) ?>

          <?php ActiveForm::end(); ?>
        </div>
    </div>
</div>


