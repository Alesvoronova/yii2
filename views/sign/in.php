<?php
/**
 * @link      http://industrialax.com/
 * @email     xristmas365@gmail.com
 * @author    isteil
 * @copyright Copyright (c) 2020 INDUSTRIALAX SOLUTIONS LLC
 * @license   https://industrialax.com/license
 */

use yii\helpers\Html;
use kartik\form\ActiveForm;

?>


<div class="py-5 main-screen align-items-stretch">
	<?php $form = ActiveForm::begin() ?>
	
	<?= $form->field($model, 'email') ?>
	<?= $form->field($model, 'password')->passwordInput() ?>
	<?= $form->field($model, 'remember')->checkbox() ?>
	
	<?= Html::submitButton('Sign In', ['class' => 'btn btn-primary']) ?>
	
	<?php ActiveForm::end() ?>
</div>
