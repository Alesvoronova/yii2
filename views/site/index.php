<?php

/* @var $this yii\web\View */

use yii\helpers\Html;

?>
<div class="site-index">
	<div class="jumbotron">
		<h1>Congratulations!</h1>
		<p>IndustrialAX-powered Application.</p>
		<p>
			<?php if(Yii::$app->user->isGuest) : ?>
				<?= Html::a('Sign Up', ['/sign/up'], ['class' => 'btn btn-primary']) ?>
				<?= Html::a('Sign In', ['/sign/in'], ['class' => 'btn btn-primary']) ?>
				<?= Html::a('Join', ['/sign/join'], ['class' => 'btn btn-primary']) ?>
			<?php else : ?>
				<?= Html::a('Dashboard', ['/admin/dashboard/index'], ['class' => 'btn btn-primary']) ?>
			<?php endif ?>
		
		</p>
	</div>
</div>
