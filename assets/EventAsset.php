<?php

namespace app\assets;


use yii\bootstrap4\BootstrapPluginAsset;
use yii\web\AssetBundle;

class EventAsset extends AssetBundle
{
	public $basePath = '@webroot';
	
	public $baseUrl  = '@web';

    public $js = [
        'js/calendar.js',
    ];

    public $depends = [
        CalendarAsset::class,
        BootstrapPluginAsset::class,
    ];
}
