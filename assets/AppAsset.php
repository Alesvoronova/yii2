<?php
/**
 * @link      http://industrialax.com/
 * @email     xristmas365@gmail.com
 * @author    isteil
 * @copyright Copyright (c) 2020 INDUSTRIALAX SOLUTIONS LLC
 * @license   https://industrialax.com/license
 */

namespace app\assets;

use yii\web\AssetBundle;

class AppAsset extends AssetBundle
{
	
	public $basePath = '@webroot';
	
	public $baseUrl  = '@web';
	
	public $css      = [
		'dist/frontend.css',
	];
	
	public $js       = [
		'dist/frontend.js'
	];
	
	public $depends  = [
		BootstrapAsset::class
	];
}
