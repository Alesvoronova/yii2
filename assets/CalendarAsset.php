<?php

namespace app\assets;


use yii\web\AssetBundle;

class CalendarAsset extends AssetBundle
{
    public $sourcePath = '@app/node_modules/@fullcalendar';

    public $css = [
        'core/main.min.css',
        'bootstrap/main.min.css',
        'daygrid/main.min.css',
        'list/main.min.css',
        'timegrid/main.min.css',
        'timeline/main.min.css',
    ];

    public $js = [
        'core/main.min.js',
        'bootstrap/main.min.js',
        'daygrid/main.min.js',
        'list/main.min.js',
        'timegrid/main.min.js',
        'timeline/main.min.js',
        'interaction/main.min.js',
    ];
}
